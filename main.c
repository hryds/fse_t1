#include <pthread.h>
#include <stdio.h>
#include <wiringPi.h>
#include <stdlib.h>

void* modo(void* numModo){
    int num = *((int *)numModo);

    digitalWrite (1, LOW) ;
    digitalWrite (26, LOW) ;
    digitalWrite (21, LOW) ;
    digitalWrite (20, LOW) ;
    digitalWrite (16, LOW) ;
    digitalWrite (12, LOW) ;
    digitalWrite (2, LOW) ;
    digitalWrite (3, LOW) ;
    digitalWrite (11, LOW) ;
    digitalWrite (0, LOW) ;
    digitalWrite (6, LOW) ;
    digitalWrite (5, LOW) ;

    if(num == 3){
        for(;;){
            digitalWrite (26, HIGH) ;
            digitalWrite (16, HIGH) ;
            digitalWrite (3, HIGH)  ;
            digitalWrite (5, HIGH)  ;
            delay(500);
            digitalWrite (26, LOW) ;
            digitalWrite (16, LOW) ;
            digitalWrite (3, LOW)  ;
            digitalWrite (5, LOW)  ;
            delay(500);
        }
    }
    if(num == 4){
        for(;;){
            digitalWrite (20, HIGH)  ;
            digitalWrite (0, HIGH)  ;
            digitalWrite (21, HIGH)  ;
            digitalWrite (11, HIGH)  ;
            delay(10);
        }
    }

    return NULL;
    
}

void* semaforo(void* numCruzamento){

    int cruzamento = *((int *)numCruzamento);

    if(cruzamento == 3){
        for (;;){

            //estado 1
            digitalWrite (21, HIGH) ;
            digitalWrite (12, HIGH) ;
            system("omxplayer example.mp3");
            delay (1000) ;

            //estado 2
            digitalWrite (1, HIGH) ;
            digitalWrite (21, LOW) ;
            delay (10000) ;

            //estado 3
            digitalWrite (1, LOW) ;
            digitalWrite (26, HIGH) ;
            system("omxplayer example.mp3");
            delay (3000) ;

            //estado 4
            digitalWrite (26, LOW) ;
            digitalWrite (21, HIGH) ;
            system("omxplayer example.mp3");
            delay (1000) ;

            //estado 5
            digitalWrite (12, LOW) ;
            digitalWrite (20, HIGH) ;
            delay (20000) ;

            //estado 6
            digitalWrite (20, LOW) ;
            digitalWrite (16, HIGH) ;
            delay (3000) ;
            system("omxplayer example.mp3");
            digitalWrite (16, LOW) ;
        }
    }
    else{
        for (;;){

            //estado 1
            digitalWrite (11, HIGH) ;
            digitalWrite (0, HIGH) ;
            delay (20000) ;

            //estado 2
            digitalWrite (5, HIGH) ;
            digitalWrite (0, LOW) ;
            system("omxplayer example.mp3");
            delay (3000) ;

            //estado 3
            digitalWrite (5, LOW) ;
            digitalWrite (6, HIGH) ;
            system("omxplayer example.mp3");
            delay (1000) ;

            //estado 4
            digitalWrite (11, LOW) ;  
            digitalWrite (2, HIGH) ; 
            delay (10000) ;

            //estado 5
            digitalWrite (2, LOW) ; 
            digitalWrite (3, HIGH) ; 
            system("omxplayer example.mp3");   
            delay (3000) ;

            //estado 6
            digitalWrite (3, LOW) ; 
            digitalWrite (11, HIGH) ; 
            system("omxplayer example.mp3");
            delay (1000) ;
            digitalWrite (6, LOW) ; 
            digitalWrite (11, LOW) ; 

        } 
    }

    return NULL;
}

int main (){

    wiringPiSetupGpio() ;

    //// cruzamento 3
    pinMode (1, OUTPUT) ; // verde
    pinMode (26, OUTPUT) ; // amarelo
    pinMode (21, OUTPUT) ; // vermelho

    pinMode (20, OUTPUT) ; // verde
    pinMode (16, OUTPUT) ; // amarelo
    pinMode (12, OUTPUT) ; // vermelho

    //// cruzamento 4
    pinMode (2, OUTPUT) ; // verde
    pinMode (3, OUTPUT) ; // amarelo
    pinMode (11, OUTPUT) ; // vermelho

    pinMode (0, OUTPUT) ; // verde
    pinMode (5, OUTPUT) ; // amarelo
    pinMode (6, OUTPUT) ; // vermelho

    pthread_t thread1_id;
    pthread_t thread2_id;
    pthread_t thread3_id;

    int cruz3 = 3;
    int cruz4 = 4;
    int resposta;

    int modoNoturno = 3;
    int modoEmergencia = 4;

    while(1){
        printf("\n--Controle de trânsito--\n");
        printf("Escolha uma opção:\n");
        printf("[1] Modo padrão\n");
        printf("[2] Modo de emergência\n");
        printf("[3] Modo noturno\n");
        printf("[4] Encerrar\n");
        scanf("%d", &resposta);

        switch (resposta){
            case 4:
                return 0;
            case 1: 
                printf("\n--Modo padrão--\n");
                pthread_create (&thread1_id, NULL, &semaforo, &cruz3);
                pthread_create (&thread2_id, NULL, &semaforo, &cruz4);

                resposta = -1;
                while(resposta!=1){
                    printf("[1] Encerrar\n");
                    scanf("%d", &resposta);
                    if(resposta==1){
                        pthread_cancel(thread1_id);
                        pthread_cancel(thread2_id);
                    }else{
                        printf("\n!Opção inválida!\n\n");
                    }
                }

                pthread_join (thread1_id, NULL);
                pthread_join (thread2_id, NULL);
                break;

            case 2:
                printf("\n--Modo de emergência--\n");
                pthread_create (&thread3_id, NULL, &modo, &modoEmergencia);

                resposta = -1;
                while(resposta!=1){
                    printf("[1] Encerrar\n");
                    scanf("%d", &resposta);
                    if(resposta==1){
                        pthread_cancel(thread3_id);
                    }else{
                        printf("\n!Opção inválida!\n\n");
                    }
                }

                pthread_join (thread3_id, NULL);
                break;

            case 3:
                printf("\n--Modo noturno--\n");
                pthread_create (&thread3_id, NULL, &modo, &modoNoturno);

                resposta = -1;
                while(resposta!=1){
                    printf("[1] Encerrar\n");
                    scanf("%d", &resposta);
                    if(resposta==1){
                        pthread_cancel(thread3_id);
                    }else{
                        printf("\n!Opção inválida!\n\n");
                    }
                }
                pthread_join (thread3_id, NULL);
                break;

            default:
                printf("--Opção inválida--\n");
                break;
        }
    }

}
